// console.log("Hello it's me");

// MINI-ACTIVITY
/*
1. Using the ES6 update, get the cube of 8.
2. Print the result on the console with the message: 'The cube of 8 is' + result
3. Use the Template Literal in printing out the message

console print:
	The cube of 8 is + result
*/
const cubeOfEight = 8 ** 3;
console.log(`The cube of 8 is ${cubeOfEight}`);

// MINI-ACTIVITY
/*
1. Destructure the address array
2. Print the values in the console: I live at 258 Washington Avenue, California, 99011
3. Use template literals
4. Send the output on hangouts
*/
const address = ["258", "Washington Ave NW", "California", "99011"]

const [no, street, city, zip] = address;
console.log(`I live at ${no} ${street}, ${city}, ${zip}`);

// MINI-ACTIVITY
/*
1. Destructure the animal array
2. Print the values in the console: 'Lolong was a saltwater crocodile. He weighed at 1075 kgs with a measurement of 20 ft 3 in'
3. Use template literals
4. Send the output on hangouts
*/

const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: '20ft 3 in'
}

const { name, species, weight, measurement} = animal;
console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}.`);

// MINI-ACTIVITY
/*
1. Loop through the numbers using forEach using arrow function
2. Print the number in the console
3. Use. the .reduce operator on the numbers array
4. Assign the result on a variable
5. Print the variable on the console
*/

let numbers = [1, 2, 3, 4, 5];
numbers.forEach((number) => console.log(number));
let x = numbers.reduce((y, z) => (z+y));
console.log(x);

// MINI-ACTIVITY
/*
1. Create a "dog" class
2. Inside of the class "dog", have a name, age, and breed
3. Instantiate a new dog class and print in the console
4. Send the screenshot of the output on hangouts
*/

class dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new dog('Garp', 78, 'Retired Vice Admiral')
console.log(myDog);

/* ACTIVITY s24 */